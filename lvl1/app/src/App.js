import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import logo from './file.svg';
import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = {
      lastFileUploaded: null,
    }
  }
  render() {
    return (
      <div className="App">
        {this.renderAlert()}
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Upload a file below</h1>
        </header>
        <div className="App-body">
          <Dropzone className="App-dropzone" onDrop={this.handleDrop}><p className="App-dropzone-text">Drop a file here</p></Dropzone>
        </div>
      </div>
    );
  }

  renderAlert() {
    const {lastFileUploaded} = this.state;
    const classes = [
      'App-alert',
      lastFileUploaded ? 'App-alert--visible' : ''
    ].join(' ');

    const message = lastFileUploaded ? `${lastFileUploaded.name} was dropped in the zone` : ''

    return (
      <div className={classes} onClick={this.handleClickAlert}>
        <span>{message}</span>
      </div>
    )
  }

  handleDrop = (acceptedFiles) => {
    if (acceptedFiles.length)
      this.setState({ lastFileUploaded: acceptedFiles[0]});
  }

  handleClickAlert = () => {
    this.setState({lastFileUploaded: null})
  }
}

export default App;
