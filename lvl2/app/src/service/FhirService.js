export function uploadDocument(file) {
  return fetch('https://fhirtest.uhn.ca/baseDstu3/Binary', { method: 'POST', body: file });
}