import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import {uploadDocument} from './service/FhirService';

import logo from './file.svg';
import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = {
      lastFileUploaded: null,
      error: null,
      totalFiles: 0,
    }
  }
  render() {
    return (
      <div className="App">
        {this.renderAlert()}
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Upload a file below</h1>
        </header>
        <div className="App-body">
          <Dropzone className="App-dropzone" onDrop={this.handleDrop}><p className="App-dropzone-text">Drop a file here</p></Dropzone>
        </div>
      </div>
    );
  }

  renderAlert() {
    const {lastFileUploaded, error, totalFiles} = this.state;
    const classes = [
      'App-alert',
      lastFileUploaded || error ? 'App-alert--visible' : '',
      error ? 'App-alert--error' : ''
    ].join(' ');

    let message = '';

    if (lastFileUploaded)
      message = `${lastFileUploaded.name} was uploaded successfully. There is currently ${totalFiles} file(s) in the cloud`
    if (error)
      message = `Something went wrong`;

    return (
      <div className={classes} onClick={this.handleClickAlert} testid="APP_ALERT">
        <span>{message}</span>
      </div>
    )
  }

  handleDrop = (acceptedFiles) => {
    if (!acceptedFiles.length)
      return;

    const file = acceptedFiles[0];

    return uploadDocument(file)
      .then((nbFiles) => {
        this.setState({ lastFileUploaded: acceptedFiles[0], error: null, totalFiles: nbFiles});
      })
      .catch((err) => {
        this.setState({error: err});
      });
  }

  handleClickAlert = () => {
    this.setState({lastFileUploaded: null, error: null})
  }
}

export default App;
