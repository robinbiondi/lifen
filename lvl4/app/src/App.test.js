import React from 'react';
import ReactDOM from 'react-dom';
import renderer from 'react-test-renderer';
import Dropzone from 'react-dropzone';
import App from './App';
import fetch from 'jest-fetch-mock';



describe('test App', () => {
  beforeEach(() => {
    global.fetch = fetch;
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('renders correctly', () => {
    const tree = renderer
      .create(<App />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });


  it('display a success message when request success', () => {
    const tree = renderer
      .create(<App />);

    fetch.mockResponseOnce();
    fetch.mockResponseOnce(JSON.stringify({
      total: 12
    }));

    const dropzone = tree.root.findByType(Dropzone);

    return dropzone.props.onDrop([new File([""], "filename")])
      .then(() => {
        const alert = tree.root.findByProps({testid: 'APP_ALERT'});

        expect(alert).toBeTruthy();
        expect(tree.root.findByType(App).instance.state.totalFiles).toEqual(12);
        expect(alert.props.className.trim()).toEqual(['App-alert', 'App-alert--visible'].join(' ').trim());
      });
  });

  it('display an error message when request fails', () => {
    const tree = renderer
      .create(<App />);

    fetch.mockRejectOnce(new Error('Fake error'));

    const dropzone = tree.root.findByType(Dropzone);

    return dropzone.props.onDrop([new File([""], "filename")])
      .then(() => {
        const alert = tree.root.findByProps({testid: 'APP_ALERT'});

        expect(alert).toBeTruthy();
        expect(alert.props.className.trim()).toEqual(['App-alert', 'App-alert--visible', 'App-alert--error'].join(' ').trim());
      });
  });
})