import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import {connect} from 'react-redux';
import {uploadDocument} from '../service/FhirService';
import {listenToFolderDrop, isFileValid} from '../service/FolderService';
import Header from '../components/Header/Header';
import Alert from '../components/Alert/Alert';
import History from '../components/History/History';
import {fileUploaded, fetchedTotalFiles, alertSeen, errorHappened, startedLoading, finishedLoading} from '../redux/actions';

import './App.css';
import loader from './loader.svg';


const mapDispatchToProps = (dispatch) => ({
  uploadedFile: (filename, size) => dispatch(fileUploaded(filename, size)),
  startedLoading: () => dispatch(startedLoading()),
  finishedLoading: () => dispatch(finishedLoading()),
  fetchedTotalFiles: (totalFiles) => dispatch(fetchedTotalFiles(totalFiles)),
  alertSeen: () => dispatch(alertSeen()),
  errorHappened: (message) => dispatch(errorHappened(message)),
});

const mapStateToProp = (state) => ({
  lastFileUploaded: state.lastFileUploaded,
  totalFiles: state.totalFiles,
  error: state.error,
  isLoading: state.isLoading,
})
export class App extends Component {

  componentWillMount() {
    this.folderListener = listenToFolderDrop(this.uploadDocument);
  }

  componentWillUnmount() {
    if(this.folderListener)
      this.folderListener.close();
  }

  render() {

    return (
      <div className="App">
        {this.renderAlert()}
        <Header />
        <div className="App-body">
          {this.renderDropZone()}
        </div>
        <History/>
      </div>
    );
  }

  renderDropZone() {
    const { isLoading } = this.props;

    if(isLoading)
      return (<img src={loader} className="App-loader" alt="logo" />);

    return (<Dropzone className="App-dropzone" onDrop={this.handleDrop}><p className="App-dropzone-text">Drop a file here</p></Dropzone>)
  }

  renderAlert() {
    const {lastFileUploaded, totalFiles, error} = this.props;

    let message = '';

    if (lastFileUploaded)
      message = `${lastFileUploaded} was uploaded successfully. There is currently ${totalFiles} file(s) in the cloud`


    return (
      <Alert
        info={message}
        error={error}
        onClickAlert={this.handleClickAlert}
      />
    )
  }

  handleDrop = (acceptedFiles) => {
    if (!acceptedFiles.length)
      return Promise.resolve();

    const file = acceptedFiles[0];

    return this.uploadDocument(file);

  }

  handleClickAlert = () => {
    const { alertSeen } = this.props;

    alertSeen();
  }

  uploadDocument = (file) => {
    const { uploadedFile, fetchedTotalFiles, errorHappened, alertSeen, startedLoading, finishedLoading } = this.props;
    alertSeen();

    return isFileValid(file)
      .then((isValid) => {
        if (!isValid) {
          errorHappened('The file needs to be a pdf and its size needs to be under 2mo')

          return Promise.resolve();
        }
        startedLoading();

        return uploadDocument(file)
          .then((nbFiles) => {
            uploadedFile(file.name, file.size)
            fetchedTotalFiles(nbFiles)
            finishedLoading();
          })
          .catch(() => {
            errorHappened('Something went wrong')
            finishedLoading();
          })
      });
  }
}

export default connect(mapStateToProp, mapDispatchToProps)(App);
