const FOLDER_NAME = 'FHIR';
const ACCEPTED_FILE_TYPES = [
  '25504446' //pdf
]
const MAX_SIZE = 2000000

/**
 * watch for files in folder FHIR and call callback everytime a file is added in it
 * @param {Function} cb The callback function
 * @returns {FSWatcher} The watcher
 */
export function listenToFolderDrop(cb) {
  if (!window.require)
    return;
  const electron = window.require('electron');
  const fs = electron.remote.require('fs');
  const path = `${electron.remote.app.getPath('documents')}/${FOLDER_NAME}`

  if (!fs.existsSync(path))
    fs.mkdirSync(path);

  return fs.watch(path, (eventType, filename) => {

    if (eventType === 'rename' && fs.existsSync(`${path}/${filename}`)) {
      const data = fs.readFileSync(`${path}/${filename}`);

      let bytes = []

      data.forEach((byte) => {
          bytes.push(byte.toString(16))
      })

      const file = new File([data], filename, {
        type: 'application/pdf'
      });

      cb(file);
    }
  });
}

/**
 * Check if a ByteArray got a valid magic number and if its size is valid
 * @param {Uint8Array} data A byteArray
 * @returns {Boolean}
 */
export function isByteArrayValid(data) {
  let signature;
  let bytes = []

  data.forEach((byte) => {
      bytes.push(byte.toString(16))
  })

  signature = bytes.join('').toUpperCase().slice(0,8)

  return ACCEPTED_FILE_TYPES.includes(signature) && data.byteLength <= MAX_SIZE;
}

/**
 * Check if a file is valid by checking its size and magic number
 * @param {File} file The file to check
 * @returns {Promise<Boolean>}
 */
export function isFileValid(file) {
  return convertFileToByteArray(file)
    .then(isByteArrayValid);
}

/**
 * Convert a file to a ByteArray
 * @param {File} file
 * @returns {Promise<Uint8Array>}
 */
export function convertFileToByteArray(file) {
  const filereader = new FileReader()

  return new Promise((resolve) => {
    filereader.onloadend = function(evt) {
      if (evt.target.readyState === FileReader.DONE) {
        const uint = new Uint8Array(evt.target.result)
        resolve(uint);
      }
    }

    filereader.readAsArrayBuffer(file);
  });
}
