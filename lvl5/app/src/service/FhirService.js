/**
 * Upload a file on the Fhir server and return the number of files uploaded
 * @param {File} file the file to upload
 * @returns {Promise<number>} The number of file on the server
 */
export function uploadDocument(file) {
  return fetch('https://fhirtest.uhn.ca/baseDstu3/Binary', { method: 'POST', body: file })
    .then(getTotalFiles);
}

/**
 * Get the number of binaries uploaded on the server
 * @returns {Promise<number>} The number of file on the server
 */
export function getTotalFiles() {
  return fetch('https://fhirtest.uhn.ca/baseDstu3/Binary?_count=1', { method: 'GET'})
    .then((res) => res.json())
    .then(res => res.total);
}