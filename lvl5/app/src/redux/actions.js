export const FILE_UPLOADED = 'FILE_UPLOADED';
export function fileUploaded(filename, size) {
  return {
    type: FILE_UPLOADED,
    filename,
    date: new Date(),
    size,
  }
}

export const FETCHED_TOTAL_FILES = 'FETCHED_TOTAL_FILES';
export function fetchedTotalFiles(totalFiles) {
  return {
    type: FETCHED_TOTAL_FILES,
    totalFiles,
  }
}

export const ALERT_SEEN = 'ALERT_SEEN';
export function alertSeen() {
  return {
    type: ALERT_SEEN,
  }
}

export const ERROR_HAPPENED = 'ERROR_HAPPENED';
export function errorHappened(message) {
  return {
    type: ERROR_HAPPENED,
    message,
  }
}

export const STARTED_LOADING = 'STARTED_LOADING';
export function startedLoading() {
  return {
    type: STARTED_LOADING,
  }
}

export const FINISHED_LOADING = 'FINISHED_LOADING';
export function finishedLoading() {
  return {
    type: FINISHED_LOADING,
  }
}

export const TOGGLE_HISTORY = 'TOGGLE_HISTORY';
export function toggleHistory() {
  return {
    type: TOGGLE_HISTORY,
  }
}