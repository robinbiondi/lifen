import {
  FILE_UPLOADED,
  FETCHED_TOTAL_FILES,
  ALERT_SEEN,
  ERROR_HAPPENED,
  STARTED_LOADING,
  FINISHED_LOADING,
  TOGGLE_HISTORY
} from './actions';

const defaultState = {
  uploadedFiles: [],
  totalFiles: 0,
  lastFileUploaded: null,
  error: null,
  isLoading: false,
  isHistoryVisible: false,
}

export default function reducer(state=defaultState, action) {
  switch(action.type) {
    case FILE_UPLOADED:
      return Object.assign({}, state, {
        uploadedFiles: [
          ...state.uploadedFiles,
          {
            filename: action.filename,
            date    : action.date,
            size    : action.size,
          }
        ],
        lastFileUploaded: action.filename,
      })
    case FETCHED_TOTAL_FILES:
      return Object.assign({}, state, {
        totalFiles: action.totalFiles,
      });
    case ALERT_SEEN:
      return Object.assign({}, state, {
        lastFileUploaded: null,
        error: null,
      });
    case ERROR_HAPPENED:
      return Object.assign({}, state, {
        error: action.message,
      });

    case STARTED_LOADING:
      return Object.assign({}, state, {
        isLoading: true,
      });

    case FINISHED_LOADING:
      return Object.assign({}, state, {
        isLoading: false,
      });

    case TOGGLE_HISTORY:
      return Object.assign({}, state, {
        isHistoryVisible: !state.isHistoryVisible,
      });
    default:
      return state;
  }
};