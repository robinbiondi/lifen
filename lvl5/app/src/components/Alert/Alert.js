import React from 'react';

export default ({info, error, onClickAlert}) => {
  const classes = [
    'App-alert',
    info || error ? 'App-alert--visible' : '',
    error ? 'App-alert--error' : ''
  ].join(' ');

  let message = '';

  message = error || info

  return (
    <div className={classes} onClick={onClickAlert} testid="APP_ALERT">
      <span>{message}</span>
    </div>
  )
}