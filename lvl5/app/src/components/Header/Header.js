import React from 'react';
import {connect} from 'react-redux';

import { toggleHistory} from '../../redux/actions';

import logo from './file.svg';
import './Header.css';

const  mapStateToProps = (state) => ({
  nbFileUploaded: state.uploadedFiles.length,
});

const  mapDispatchToProps = (dispatch) => ({
  toggleHistory: () => dispatch(toggleHistory()),
});

export  function Header(props) {
  const { nbFileUploaded, toggleHistory } = props;
  return (
    <header className="Header">
      <img src={logo} className="Header-logo" alt="logo" />
      <h1 className="Header-title">Upload a file below</h1>
      <div className="Header-count" onClick={toggleHistory}>
        <span>{nbFileUploaded}</span>
      </div>
    </header>
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);