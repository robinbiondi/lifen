import React from 'react';
import {connect} from 'react-redux';

import { toggleHistory} from '../../redux/actions';

import './History.css';

const  mapStateToProps = (state) => ({
  uploadedFiles: state.uploadedFiles,
  isHistoryVisible: state.isHistoryVisible,
});

const  mapDispatchToProps = (dispatch) => ({
  toggleHistory: () => dispatch(toggleHistory()),
});

export  function Header(props) {
  const { uploadedFiles, isHistoryVisible, toggleHistory } = props;

  const classes = [
    'History',
    isHistoryVisible ? 'History--visible' : ''
  ].join(' ');

  return (
    <div className={classes}>
      <div className="History-close" onClick={toggleHistory}>Close</div>
      <h3> The files you sent</h3>
      <div className="History--items">
        {uploadedFiles.map((file) => {
          return (<Item
            item={file}
            key={file.filename + file.date.toString()}
          />);
        })}
      </div>
    </div>
  );
}

function Item({item}) {
    return (
      <div className="History-item">
        <div>{item.filename}</div>
        <div>{`${item.size/1000} Ko`}</div>
        <div>{`${item.date.getDate()}/${item.date.getMonth()}`}</div>
      </div>
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);